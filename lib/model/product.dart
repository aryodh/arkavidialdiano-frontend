class Product {
  final String nama;
  final int hargaBibit;
  final int persen;
  final String body;
  final int durasi;
  final String risiko;
  final int rating;
  final int jumlahPenjualan;
  final String namaOwner;
  final int jasa;
  final int alat;
  final int pupuk;
  final String image1;
  final String image2;
  final String image3;
  int harga;

  Product(
    this.nama,
    this.hargaBibit,
    this.persen,
    this.body,
    this.durasi,
    this.risiko,
    this.rating,
    this.jumlahPenjualan,
    this.namaOwner,
    this.jasa,
    this.alat,
    this.pupuk,
    this.image1,
    this.image2,
    this.image3,
  ) {
    this.harga = this.hargaBibit + this.jasa + this.pupuk;
  }
}
