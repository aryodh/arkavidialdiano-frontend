import 'package:arkavidialdiano_front/pages/trackerDetailPage.dart';
import 'package:arkavidialdiano_front/widget/BottomNavbar.dart';
import 'package:arkavidialdiano_front/widget/TMPCard.dart';
import 'package:flutter/material.dart';
import '../constants.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class TrackerMainPage extends StatefulWidget {
  @override
  _TrackerMainPageState createState() => _TrackerMainPageState();
}

class _TrackerMainPageState extends State<TrackerMainPage> {
  final List<Widget> widgetRender = [];
  List trackerData;
  List bibitData;
  bool isLoading;

  void _cardClick() {
    Navigator.of(context).push(
      new MaterialPageRoute(
        builder: (BuildContext context) => new TrackerDetailPage(),
      ),
    );
  }

  Future getData() async {
    setState(() {
      isLoading = true; //Data is loading
    });
    http.Response response = await http.get(
      "http://grow-backend.herokuapp.com/investasi/users/1/",
      headers: {
        "Authorization": "token 8c3912d697f40ec870fb5e12f779b5ab85222d4c"
      },
    );
    http.Response responseBibit = await http.get(
      "http://grow-backend.herokuapp.com/bibit/?format=json",
      headers: {
        "Authorization": "token 8c3912d697f40ec870fb5e12f779b5ab85222d4c"
      },
    );
    debugPrint(response.body);
    setState(() {
      bibitData = json.decode(responseBibit.body);
      trackerData = json.decode(response.body);
    });
    setState(() {
      isLoading = false; //Data is loading
    });
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> listWidget = [];
    int banyaknya = 4;
    // if (trackerData.length != null) {
    //   banyaknya = trackerData.length;
    // }
    List<Map> theData = [
      {
        "title": "Gelombang Cinta Kalimantan",
        "price": 50000,
      },
      {
        "title": "Bunga Raflesia",
        "price": 75000,
      },
      {
        "title": "Melon Jepang",
        "price": 43000,
      },
      {
        "title": "Bonsai",
        "price": 320000,
      },
    ];
    for (var i = 0; i < theData.length; i++) {
      var kartu = TMPCard(
        nama: theData[i]['title'],
        total: theData[i]['price'],
      );
      listWidget
          .add(new GestureDetector(onTap: () => _cardClick(), child: kartu));
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Tracker',
          style: TextStyle(color: Colors.black),
        ),
        automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
      ),
      body: SafeArea(
        child: ListView(
          children: <Widget>[
            Container(
              height: 250,
              child: Row(
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width / 2,
                    child: Container(
                      child: Image.asset("assets/images/trackerAsset.png"),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width / 2,
                    child: Column(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(
                            top: 40,
                          ),
                          alignment: Alignment.topLeft,
                          child: Text(
                            "Total Pendaptan (Rp)",
                            style: TextStyle(
                              fontSize: 12,
                            ),
                          ),
                        ),
                        Container(
                          alignment: Alignment.topLeft,
                          child: Text(
                            "524.000",
                            style: TextStyle(
                              fontSize: 24,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                            top: 20,
                          ),
                          alignment: Alignment.topLeft,
                          child: Text(
                            "Total Pendapatan (Rp)",
                            style: TextStyle(
                              fontSize: 12,
                            ),
                          ),
                        ),
                        Container(
                          alignment: Alignment.topLeft,
                          child: Text(
                            "524.000",
                            style: TextStyle(
                              fontSize: 24,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                            top: 20,
                          ),
                          alignment: Alignment.topLeft,
                          child: Text(
                            "Investasi Aktif",
                            style: TextStyle(
                              fontSize: 12,
                            ),
                          ),
                        ),
                        Container(
                          alignment: Alignment.topLeft,
                          child: Text(
                            "10",
                            style: TextStyle(
                              fontSize: 24,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              child: Column(
                children: listWidget,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
