import 'dart:convert';

import 'package:arkavidialdiano_front/pages/articleDetailPage.dart';
import 'package:arkavidialdiano_front/widget/AMPCard.dart';
import 'package:arkavidialdiano_front/widget/AMPCarousel.dart';
import 'package:arkavidialdiano_front/widget/BottomNavbar.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import '../constants.dart';
import 'dart:async' show Future;
import 'package:flutter/services.dart' show rootBundle;

class ArticleMainPage extends StatefulWidget {
  // Future<String> loadAsset() async {
  //   return await rootBundle.loadString('assets/json/article.json');
  // }
  @override
  _ArticleMainPageState createState() => _ArticleMainPageState();
}

class _ArticleMainPageState extends State<ArticleMainPage> {
  void _carouselClick(Map objArticle) {
    Navigator.of(context).push(
      new MaterialPageRoute(
        builder: (BuildContext context) => new ArticleDetailPage(objArticle),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Article',
        ),
        automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
      ),
      body: SafeArea(
        child: FutureBuilder(
            future: DefaultAssetBundle.of(context)
                .loadString('assets/json/article.json'),
            builder: (context, snapshot) {
              // Decode the JSON
              var newData = json.decode(snapshot.data);

              List<Widget> list_card = [];
              List<Widget> list_carousel = [];
              for (var i = 0; i < newData.length; i++) {
                var kartu = new AMPCard(
                  title: newData[i]['title'],
                  date: newData[i]['date'],
                  image: newData[i]['image'],
                  kategori: "Investasi",
                );
                list_card.add(new GestureDetector(
                  onTap: () => _carouselClick(newData[i]),
                  child: kartu,
                ));
                if (i < 3) {
                  var carous = new AMPCarousel(
                    title: newData[i]['title'].substring(0, 20) + "...",
                    date: newData[i]['date'],
                    image: newData[i]['image'],
                  );
                  list_carousel.add(new GestureDetector(
                    onTap: () => _carouselClick(newData[i]),
                    child: carous,
                  ));
                }
              }

              return ListView(
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.symmetric(
                          horizontal: 16,
                          vertical: 16,
                        ),
                        alignment: Alignment.topLeft,
                        child: Text(
                          "Pilihan terbaik untukmu",
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        width: double.infinity,
                        child: CarouselSlider(
                            viewportFraction: 0.8,
                            height: 180,
                            items: list_carousel),
                      ),
                      // AMPCarousel(),
                      Container(
                        margin: EdgeInsets.symmetric(
                          vertical: 5,
                          horizontal: 16,
                        ),
                        alignment: Alignment.topLeft,
                        child: Text(
                          "Semua Artikel",
                          style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(
                          horizontal: 16,
                        ),
                        child: Divider(
                          color: Colors.black,
                        ),
                      ),
                      Container(
                        child: Column(
                          children: list_card,
                        ),
                      ),
                    ],
                  ),
                ],
              );
            }),
      ),
    );
  }
}
