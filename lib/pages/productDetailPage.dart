import 'package:arkavidialdiano_front/widget/HPCarouselPromo.dart';
import 'package:arkavidialdiano_front/widget/HPCatalogueCard.dart';
import 'package:arkavidialdiano_front/widget/PDPCostCalculation.dart';
import 'package:arkavidialdiano_front/widget/PDPSummaryCard.dart';
import 'package:arkavidialdiano_front/widget/QuantityWidget.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'dart:async';
import 'dart:convert';
import '../constants.dart';

import 'package:http/http.dart' as http;
import 'package:arkavidialdiano_front/model/product.dart';

import 'mainPage.dart';

class ProductDetailPage extends StatefulWidget {
  final int id;
  final Product prod1;

  ProductDetailPage(this.id, this.prod1);

  @override
  _ProductDetailPageState createState() => _ProductDetailPageState();
}

class _ProductDetailPageState extends State<ProductDetailPage> {
  // Map bibitDetailData;
  bool isLoading = false;
  int _n = 0;
  Future getData() async {
    setState(() {
      isLoading = true; //Data is loading
    });
    // String link = "http://grow-backend.herokuapp.com/bibit/" +
    //     widget.id.toString() +
    //     "?format=json";
    // var response = await http.get(link, headers: {
    //   "Authorization": "token 8c3912d697f40ec870fb5e12f779b5ab85222d4c"
    // });
    // debugPrint('id =' + widget.id.toString());
    // debugPrint(response.body);
    // setState(() {
    //   bibitDetailData = json.decode(response.body);
    // });

    setState(() {
      isLoading = false; //Data has loaded
    });
  }

  @override
  void initState() {
    super.initState();
    this.getData();
  }

  @override
  Widget build(BuildContext context) {
    return isLoading
        ? Center(child: CircularProgressIndicator())
        : Scaffold(
            appBar: AppBar(
              title: Text(
                "Product Detail",
                style: TextStyle(color: Colors.black),
              ),
              backgroundColor: Colors.white,
            ),
            body: Container(
              color: Colors.white,
              child: SafeArea(
                child: ListView(
                  children: <Widget>[
                    Container(
                      height: 200,
                      child: Image.asset(
                        'assets/images/cardCatalogue.png',
                        fit: BoxFit.cover,
                      ),
                    ),
                    PDPSummaryCard(widget.prod1),
                    Container(
                      width: double.infinity,
                      child: Card(
                        child: Container(
                            padding: EdgeInsets.symmetric(
                              horizontal: 16,
                              vertical: 16,
                            ),
                            child: Column(
                              children: <Widget>[
                                Container(
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    "Hasil Produk",
                                    style: TextStyle(
                                      fontSize: 16,
                                    ),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(
                                    top: 20,
                                  ),
                                  child: Column(
                                    children: <Widget>[
                                      Container(
                                        alignment: Alignment.bottomLeft,
                                        child: Text(
                                          "Keuntungan per bibit",
                                          style: TextStyle(
                                            fontSize: 12,
                                            fontWeight: FontWeight.w300,
                                          ),
                                        ),
                                      ),
                                      Container(
                                        alignment: Alignment.bottomLeft,
                                        margin: EdgeInsets.only(
                                          top: 10,
                                        ),
                                        child: Text(
                                          (widget.prod1.hargaBibit *
                                                  widget.prod1.persen /
                                                  100)
                                              .toString(),
                                          style: TextStyle(
                                            fontSize: 24,
                                            fontWeight: FontWeight.w600,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(
                                    top: 10,
                                  ),
                                  height: 100,
                                  width: double.infinity,
                                  child: CarouselSlider(
                                      viewportFraction: 0.35,
                                      items: [1, 2, 3, 4].map((i) {
                                        return Container(
                                          child: ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            child: Image.asset(
                                              'assets/images/cardCatalogue.png',
                                              fit: BoxFit.cover,
                                              width: 100,
                                              height: 100,
                                            ),
                                          ),
                                        );
                                      }).toList()),
                                ),
                              ],
                            )),
                      ),
                    ),
                    PDPCostCalculation(widget.prod1),
                    // PDPCostCalculation(
                    //   bibitDetailData['a'],
                    //   bibitDetailData['b'],
                    //   bibitDetailData['c'],
                    // )
                  ],
                ),
              ),
            ),
            bottomNavigationBar: Container(
              height: 64,
              padding: EdgeInsets.only(
                bottom: 1,
              ),
              child: Row(
                children: <Widget>[
                  Container(
                    height: 64,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.symmetric(
                            horizontal: 15,
                          ),
                          height: 35,
                          margin: EdgeInsets.only(bottom: 3),
                          child: IconButton(
                            iconSize: 30,
                            icon: Icon(Icons.home),
                            color: Colors.grey,
                            onPressed: () {
                              Navigator.pop(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => MainPage(),
                                ),
                              );
                            },
                          ),
                        ),
                        Text(
                          "Home",
                          style: TextStyle(
                            fontSize: 12,
                            color: Colors.grey,
                          ),
                        ),
                      ],
                    ),
                  ),
                  ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: Container(
                        padding: EdgeInsets.only(bottom: 0),
                        width: 300,
                        height: 50,
                        child: FlatButton(
                          color: Theme.of(context).primaryColor,
                          textColor: Colors.white,
                          disabledColor: Colors.grey,
                          disabledTextColor: Colors.black,
                          padding: EdgeInsets.all(8.0),
                          splashColor: Colors.blueAccent,
                          onPressed: () => _settingModalBottomSheet(context),
                          child: Text(
                            "Investasi Sekarang",
                            style: TextStyle(
                              fontSize: 15.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        )),
                  )
                ],
              ),
            ),
          );
  }

  void add() {
    setState(() {
      _n++;
      widget.prod1.harga += widget.prod1.harga;
    });
  }

  void minus() {
    setState(() {
      if (_n != 0) _n--;
      if (widget.prod1.harga != 0) widget.prod1.harga -= widget.prod1.harga;
    });
  }

  void _settingModalBottomSheet(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            height: 255,
            child: new Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                        left: 20,
                        top: 20,
                        bottom: 20,
                      ),
                      child: Text(
                        "Masukkan jumlah bibit yang ingin dipesan",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width / 2,
                      child: Column(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(bottom: 16, left: 20),
                            child: Align(
                              alignment: Alignment.topLeft,
                              child: Text(
                                "Jumlah",
                                style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(vertical: 21),
                      alignment: Alignment.topRight,
                      width: MediaQuery.of(context).size.width / 2,
                      child: Column(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(bottom: 16),
                            child: Align(
                              alignment: Alignment.topRight,
                              child: new Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: <Widget>[
                                  Container(
                                    width: 30,
                                    height: 30,
                                    child: new FloatingActionButton(
                                      backgroundColor:
                                          Theme.of(context).primaryColor,
                                      elevation: 0,
                                      onPressed: minus,
                                      child: new Icon(
                                        const IconData(0xe15b,
                                            fontFamily: 'MaterialIcons'),
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                  new Text('$_n',
                                      style: new TextStyle(fontSize: 16.0)),
                                  Container(
                                    width: 30,
                                    height: 30,
                                    child: new FloatingActionButton(
                                      backgroundColor:
                                          Theme.of(context).primaryColor,
                                      elevation: 0,
                                      onPressed: add,
                                      child: new Icon(
                                        Icons.add,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width / 2,
                      child: Column(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(bottom: 16, left: 20),
                            child: Align(
                              alignment: Alignment.topLeft,
                              child: Text(
                                "Total",
                                style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width / 2 - 40,
                      child: Column(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(bottom: 16),
                            child: Align(
                              alignment: Alignment.topRight,
                              child: Text(
                                widget.prod1.harga.toString(),
                                textAlign: TextAlign.right,
                                style: TextStyle(
                                  fontSize: 24,
                                  fontWeight: FontWeight.w800,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 0),
                  width: MediaQuery.of(context).size.width,
                  height: 64,
                  child: FlatButton(
                    color: Theme.of(context).primaryColor,
                    textColor: Colors.white,
                    disabledColor: Colors.grey,
                    disabledTextColor: Colors.black,
                    padding: EdgeInsets.all(8.0),
                    splashColor: Colors.blueAccent,
                    onPressed: () => _transactionBegin(),
                    child: Text(
                      "Investasi Sekarang",
                      style: TextStyle(
                        fontSize: 15.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                )
              ],
            ),
          );
        });
  }

  Future<void> _transactionBegin() async {
    // int num = NumberInputWithIncrementDecrement.getCurrentValue();
    Navigator.pop(context);
    final uri = 'http://grow-backend.herokuapp.com/investasi/';
    var map = new Map<String, dynamic>();
    map['investor_id'] = '1';
    map['bibit_id'] = widget.id;
    map['totalInvest'] = widget.prod1.harga;
    map['totalKeuntungan'] =
        widget.prod1.harga + widget.prod1.harga * widget.prod1.persen / 100;

    http.Response response = await http.post(uri, body: map, headers: {
      "Authorization": "token 8c3912d697f40ec870fb5e12f779b5ab85222d4c"
    });
  }
}
