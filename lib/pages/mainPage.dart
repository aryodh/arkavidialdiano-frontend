import 'package:arkavidialdiano_front/widget/BottomNavbar.dart';

import '../constants.dart';
import '../widget/HPCarouselPromo.dart';
import '../widget/HPCatalogueContainer.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  List bibitData;
  bool isLoading;

  Future getData() async {
    setState(() {
      isLoading = true; //Data is loading
    });
    http.Response response = await http.get(
      "http://grow-backend.herokuapp.com/bibit/?format=json",
      headers: {
        "Authorization": "token 8c3912d697f40ec870fb5e12f779b5ab85222d4c"
      },
    );
    debugPrint(response.body);
    setState(() {
      bibitData = json.decode(response.body);
    });
    setState(() {
      isLoading = false; //Data is loading
    });
  }

  @override
  void initState() {
    super.initState();
    this.getData();
  }

  @override
  Widget build(BuildContext context) {
    return isLoading
        ? Center(child: CircularProgressIndicator())
        : Scaffold(
            appBar: AppBar(
              title: Text(
                'Grow',
                style: TextStyle(color: Colors.white),
              ),
              automaticallyImplyLeading: false,
              backgroundColor: Theme.of(context).primaryColor,
            ),
            body: SafeArea(
              child: ListView(
                children: <Widget>[
                  HPCarouselPromo(),
                  HPCatalogueContainer(bibitData),
                ],
              ),
            ), // This trailing comma makes auto-formatting nicer for build methods.
          );
  }
}
