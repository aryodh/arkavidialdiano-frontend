import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import '../constants.dart';

class TrackerDetailPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Tracker',
          style: TextStyle(color: Colors.black),
        ),
        backgroundColor: Colors.white,
      ),
      body: SafeArea(
        child: ListView(
          children: <Widget>[
            Container(
              height: 200,
              child: Image.asset(
                'assets/images/cardCatalogue.png',
                fit: BoxFit.cover,
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(
                horizontal: 16,
                vertical: 16,
              ),
              child: Text(
                "Gelombang Cinta Kalimantan",
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 24,
                ),
              ),
            ),
            CarouselSlider(height: 250, viewportFraction: 0.6, items: [
              Container(
                width: 225,
                height: 250,
                child: Card(
                  child: Container(
                    padding: EdgeInsets.symmetric(
                      horizontal: 16,
                      vertical: 16,
                    ),
                    child: Column(
                      children: <Widget>[
                        Container(
                          alignment: Alignment.topLeft,
                          child: Text(
                            "Ringkasan Investasi",
                            style: TextStyle(fontSize: 16),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                            top: 20,
                          ),
                          alignment: Alignment.topLeft,
                          child: Text(
                            "Dana Diberikan (Rp)",
                            style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                            top: 5,
                          ),
                          alignment: Alignment.topLeft,
                          child: Text(
                            "500,000",
                            style: TextStyle(
                              fontSize: 24,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                            top: 10,
                          ),
                          alignment: Alignment.topLeft,
                          child: Text(
                            "Perkiraan Keuntungan (Rp)",
                            style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                            top: 5,
                          ),
                          alignment: Alignment.topLeft,
                          child: Text(
                            "50,000",
                            style: TextStyle(
                              fontSize: 24,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                            top: 10,
                          ),
                          alignment: Alignment.topLeft,
                          child: Text(
                            "Total Dana Investasi (Rp)",
                            style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                            top: 5,
                          ),
                          alignment: Alignment.topLeft,
                          child: Text(
                            "10,000,000",
                            style: TextStyle(
                              fontSize: 24,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Container(
                height: 250,
                width: 225,
                child: Card(
                  child: Container(
                    padding: EdgeInsets.symmetric(
                      horizontal: 16,
                    ),
                    child: Column(
                      children: <Widget>[
                        Container(
                          alignment: Alignment.topLeft,
                          padding: EdgeInsets.symmetric(
                            vertical: 16,
                          ),
                          child: Text(
                            "Kondisi Produk",
                            style: TextStyle(
                              fontSize: 16,
                            ),
                          ),
                        ),
                        Container(
                          height: 130,
                          margin: EdgeInsets.only(
                            bottom: 10,
                          ),
                          child: Image.asset('assets/images/pieChart.png'),
                        ),
                        Container(
                          alignment: Alignment.topLeft,
                          child: Text(
                            "Produk sehat - 75%",
                            style: TextStyle(
                              fontSize: 12,
                              color: Colors.cyan[300],
                            ),
                          ),
                        ),
                        Container(
                          alignment: Alignment.topLeft,
                          child: Text(
                            "Produk kurang baik - 25%",
                            style: TextStyle(
                              fontSize: 12,
                              color: Colors.cyan[600],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ]),
            Container(
              margin: EdgeInsets.only(
                top: 30,
                left: 16,
                right: 16,
                bottom: 10,
              ),
              child: Text(
                "Update Produk",
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
            Container(
              width: double.infinity,
              padding: EdgeInsets.symmetric(
                horizontal: 16,
              ),
              child: Card(
                child: Container(
                    padding: EdgeInsets.symmetric(
                      horizontal: 16,
                      vertical: 16,
                    ),
                    child: Column(
                      children: <Widget>[
                        Container(
                          alignment: Alignment.topLeft,
                          child: Text(
                            "Produk Terjual",
                            style: TextStyle(
                              fontSize: 16,
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                            top: 8,
                          ),
                          child: Row(
                            children: <Widget>[
                              Container(
                                child: Text(
                                  "33",
                                  style: TextStyle(
                                    fontSize: 32,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                alignment: Alignment.bottomLeft,
                                margin: EdgeInsets.only(
                                  left: 5,
                                ),
                                child: Text(
                                  "/ 50",
                                  style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w300,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                            top: 16,
                          ),
                          alignment: Alignment.topLeft,
                          child: Text(
                            "Foto Kondisi Produk",
                            style: TextStyle(
                              fontSize: 16,
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                            top: 10,
                          ),
                          height: 100,
                          width: double.infinity,
                          child: CarouselSlider(
                              viewportFraction: 0.35,
                              items: [1, 2, 3, 4].map((i) {
                                return Container(
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(10),
                                    child: Image.asset(
                                      'assets/images/cardCatalogue.png',
                                      fit: BoxFit.cover,
                                      width: 100,
                                      height: 100,
                                    ),
                                  ),
                                );
                              }).toList()),
                        ),
                        Container(
                          alignment: Alignment.topLeft,
                          margin: EdgeInsets.only(top: 5),
                          child: Text(
                            "Last update : 25/12/2019",
                            style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                        ),
                      ],
                    )),
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                top: 30,
              ),
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: Text(
                'Kontak Penanam',
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                bottom: 20,
              ),
              padding: EdgeInsets.symmetric(
                horizontal: 16,
                vertical: 8,
              ),
              child: Row(
                children: <Widget>[
                  Container(
                    width: 70,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(40),
                      child: Image.asset(
                        'assets/images/cardCatalogue.png',
                        width: 70,
                        height: 70,
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(
                      horizontal: 10,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "Abraham William",
                          style: TextStyle(
                            fontSize: 18,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.symmetric(
                            vertical: 5,
                          ),
                          child: Text(
                            "Bapak Rumah Tangga",
                            style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.w200,
                            ),
                          ),
                        ),
                        Text(
                          "0813 4567 8910",
                          style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w300,
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
