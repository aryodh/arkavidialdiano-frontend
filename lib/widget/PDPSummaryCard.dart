import 'package:arkavidialdiano_front/model/product.dart';
import 'package:arkavidialdiano_front/widget/HPStarRating.dart';
import 'package:flutter/material.dart';

import '../constants.dart';

class PDPSummaryCard extends StatelessWidget {
  final Product prod;

  PDPSummaryCard(this.prod);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: (MediaQuery.of(context).size.width),
      height: 240,
      padding: EdgeInsets.all(0),
      child: Card(
        child: Container(
            padding: EdgeInsets.all(16),
            child: Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(bottom: 16),
                  child: Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      this.prod.nama,
                      style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                ),
                Container(
                  child: Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      "Rp" + this.prod.harga.toString(),
                      style: TextStyle(
                        fontSize: 18,
                        color: Theme.of(context).primaryColor,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 16, top: 4),
                  child: Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      this.prod.namaOwner,
                      style: TextStyle(
                        fontSize: 12,
                      ),
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(
                    top: 3,
                    bottom: 3,
                  ),
                  child: Align(
                    alignment: Alignment.topLeft,
                    child: Row(
                      children: <Widget>[
                        IconTheme(
                          data: IconThemeData(
                            color: Theme.of(context).primaryColor,
                            size: 16,
                          ),
                          child: HPStarRating(
                            value: this.prod.rating,
                          ),
                        ),
                        Container(
                          alignment: Alignment.centerLeft,
                          margin: EdgeInsets.only(
                            left: 3,
                          ),
                          child: Text(
                            "(" + this.prod.jumlahPenjualan.toString() + ")",
                            style: TextStyle(
                              fontSize: 10,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Container(
                  width: (MediaQuery.of(context).size.width),
                  margin: EdgeInsets.only(top: 30),
                  child: Row(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(left: 30, right: 30),
                        child: Column(
                          children: <Widget>[
                            Text(
                              this.prod.persen.toString() + "%",
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.w600),
                            ),
                            Text(
                              "Keuntungan",
                              style: TextStyle(fontSize: 10),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 30, right: 30),
                        child: Column(
                          children: <Widget>[
                            Text(
                              this.prod.durasi.toString(),
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.w600),
                            ),
                            Text(
                              "Durasi",
                              style: TextStyle(fontSize: 10),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 30, right: 30),
                        child: Column(
                          children: <Widget>[
                            Text(
                              this.prod.risiko.toString(),
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.w600),
                            ),
                            Text(
                              "Resiko",
                              style: TextStyle(fontSize: 10),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            )),
      ),
    );
  }
}
