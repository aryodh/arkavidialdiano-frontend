import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';

class AMPCarousel extends StatelessWidget {
  final String title;
  final String date;
  final String image;

  AMPCarousel({this.title, this.date, this.image});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 300,
      child: Card(
        elevation: 3,
        child: Column(
          children: <Widget>[
            Container(
              child: Image.network(
                this.image,
                fit: BoxFit.cover,
                height: 115,
                width: 300,
              ),
            ),
            Column(
              children: <Widget>[
                Container(
                  alignment: Alignment.topLeft,
                  margin: EdgeInsets.only(
                    left: 16,
                    top: 10,
                  ),
                  child: Text(
                    this.title,
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Container(
                  alignment: Alignment.topLeft,
                  margin: EdgeInsets.only(
                    left: 16,
                    top: 5,
                  ),
                  child: Text(
                    this.date,
                    style: TextStyle(
                      fontSize: 10,
                      fontWeight: FontWeight.w300,
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
