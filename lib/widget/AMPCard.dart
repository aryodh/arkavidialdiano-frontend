import 'package:flutter/material.dart';

class AMPCard extends StatelessWidget {
  final String kategori;
  final String title;
  final String date;
  final String image;

  AMPCard(
      {this.kategori,
      this.title,
      this.date,
      this.image = "assets/images/articleCarousel.png"});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(
        horizontal: 16,
      ),
      child: Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              height: 86,
              width: 86,
              padding: EdgeInsets.all(6),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Image.network(
                  this.image,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Container(
              alignment: Alignment.centerLeft,
              margin: EdgeInsets.symmetric(
                vertical: 5,
                horizontal: 5,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    height: 15,
                    margin: EdgeInsets.only(
                      bottom: 5,
                    ),
                    alignment: Alignment.topLeft,
                    child: Text(
                      this.kategori,
                      style: TextStyle(
                        fontWeight: FontWeight.w300,
                      ),
                    ),
                  ),
                  Container(
                    height: 30,
                    width: 250,
                    margin: EdgeInsets.only(
                      bottom: 5,
                    ),
                    alignment: Alignment.centerLeft,
                    child: Text(
                      this.title,
                      softWrap: true,
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                  Container(
                    height: 15,
                    alignment: Alignment.bottomLeft,
                    child: Text(
                      this.date,
                      style: TextStyle(
                        fontSize: 10,
                        fontWeight: FontWeight.w300,
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
