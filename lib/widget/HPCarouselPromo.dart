import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../constants.dart';

class HPCarouselPromo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).primaryColor,
      height: 200,
      width: double.infinity,
      padding: EdgeInsets.symmetric(
        vertical: 16,
      ),
      child: CarouselSlider(
        items: [1, 2, 3, 4].map((i) {
          return new Container(
            margin: EdgeInsets.symmetric(
              horizontal: 5,
            ),
            width: 400,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: Card(
                child: ClipRRect(
                  child: Image.network(
                      'https://ecs7.tokopedia.net/img/blog/promo/2020/02/EMAS-THUMBNAIL_600x328.jpg',
                      fit: BoxFit.cover),
                ),
                elevation: 5,
              ),
            ),
          );
        }).toList(),
        viewportFraction: 0.8,
      ),
    );
  }
}
