import 'package:arkavidialdiano_front/model/product.dart';
import 'package:arkavidialdiano_front/widget/HPStarRating.dart';
import 'package:flutter/material.dart';

import '../constants.dart';

class HPCatalogueCard extends StatelessWidget {
  final Product prod;
  final int id;

  HPCatalogueCard(this.prod, this.id);
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width / 2 - 5,
      child: Card(
        child: Container(
          padding: EdgeInsets.all(3),
          child: Column(
            children: <Widget>[
              Container(
                child: Container(
                  width: MediaQuery.of(context).size.width - 2,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(3),
                    child: Image.asset('assets/images/cardCatalogue.png'),
                  ),
                ),
              ),
              Container(
                alignment: Alignment.topLeft,
                margin: EdgeInsets.all(5),
                padding: EdgeInsets.only(left: 6),
                child: Text(
                  this.prod.nama,
                  style: TextStyle(
                    fontSize: 16,
                  ),
                ),
              ),
              Container(
                alignment: Alignment.centerLeft,
                margin: EdgeInsets.all(5),
                padding: EdgeInsets.only(left: 6),
                child: Text(
                  "Rp " + this.prod.harga.toString(),
                  style: TextStyle(
                    fontSize: 16,
                    color: Theme.of(context).primaryColor,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.only(left: 6),
                margin: EdgeInsets.only(
                  top: 25,
                  right: 6,
                  left: 6,
                ),
                child: Text(
                  this.prod.namaOwner,
                  style: TextStyle(
                    fontSize: 12,
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                  left: 10,
                  top: 3,
                  bottom: 3,
                ),
                child: Align(
                  alignment: Alignment.topLeft,
                  child: Row(
                    children: <Widget>[
                      IconTheme(
                        data: IconThemeData(
                          color: Theme.of(context).primaryColor,
                          size: 16,
                        ),
                        child: HPStarRating(
                          value: this.prod.rating,
                        ),
                      ),
                      Container(
                        alignment: Alignment.centerLeft,
                        margin: EdgeInsets.only(
                          left: 3,
                        ),
                        child: Text(
                          "(" + this.prod.jumlahPenjualan.toString() + ")",
                          style: TextStyle(
                            fontSize: 10,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
