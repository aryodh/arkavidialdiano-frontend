import 'package:flutter/material.dart';

class TMPCard extends StatelessWidget {
  final String nama;
  final int total;
  final String image;

  TMPCard({
    this.nama = "Produk",
    this.total = 0,
    this.image = 'assets/images/cardCatalogue.png',
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(
        horizontal: 16,
      ),
      child: Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              height: 86,
              width: 86,
              padding: EdgeInsets.all(6),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Image.asset(
                  this.image,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Container(
              alignment: Alignment.centerLeft,
              margin: EdgeInsets.symmetric(
                vertical: 5,
                horizontal: 5,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    height: 45,
                    width: 200,
                    margin: EdgeInsets.only(
                      bottom: 5,
                    ),
                    alignment: Alignment.centerLeft,
                    child: Text(
                      this.nama,
                      softWrap: true,
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                  Container(
                    height: 15,
                    alignment: Alignment.bottomLeft,
                    child: Text(
                      "Rp " + this.total.toString(),
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  )
                ],
              ),
            ),
            Container(
              width: 50,
              child: Image.asset("assets/images/arrow.png"),
            ),
          ],
        ),
      ),
    );
  }
}
