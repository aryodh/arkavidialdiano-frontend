import 'package:arkavidialdiano_front/pages/articleMainPage.dart';
import 'package:arkavidialdiano_front/pages/mainPage.dart';
import 'package:arkavidialdiano_front/pages/profilePage.dart';
import 'package:arkavidialdiano_front/pages/trackerMainPage.dart';
import 'package:flutter/material.dart';

class BottomNavbar extends StatefulWidget {
  final int idx;

  BottomNavbar(this.idx);
  @override
  _BottomNavbarState createState() => _BottomNavbarState();
}

class _BottomNavbarState extends State<BottomNavbar> {
  int _currentIndex = 0;

  Future getData() async {
    _currentIndex = widget.idx;
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });

    if (_currentIndex == 0) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => MainPage(),
        ),
      );
    }
    if (_currentIndex == 1) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => TrackerMainPage(),
        ),
      );
    }
    if (_currentIndex == 2) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ArticleMainPage(),
        ),
      );
    }
    if (_currentIndex == 3) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ProfilePage(),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      onTap: onTabTapped,
      currentIndex: _currentIndex,
      type: BottomNavigationBarType.fixed,
      items: [
        new BottomNavigationBarItem(
          icon: Icon(Icons.home),
          title: Text('Home'),
        ),
        new BottomNavigationBarItem(
          icon: Icon(Icons.track_changes),
          title: Text('Tracker'),
        ),
        new BottomNavigationBarItem(
          icon: Icon(Icons.web),
          title: Text('Article'),
        ),
        new BottomNavigationBarItem(
          icon: Icon(Icons.person),
          title: Text('Profile'),
        ),
      ],
    );
  }
}
