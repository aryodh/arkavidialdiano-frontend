import 'package:arkavidialdiano_front/model/product.dart';
import 'package:arkavidialdiano_front/widget/HPCatalogueCard.dart';
import 'package:flutter/material.dart';

import 'package:arkavidialdiano_front/pages/productDetailPage.dart';

class HPCatalogueContainer extends StatefulWidget {
  final List data;

  HPCatalogueContainer(this.data);

  @override
  _HPCatalogueContainerState createState() => _HPCatalogueContainerState();
}

class _HPCatalogueContainerState extends State<HPCatalogueContainer> {
  final List<Widget> widgetRender = [];
  final int x = 1;

  void _cardClick(int id, prod) {
    Navigator.of(context).push(
      new MaterialPageRoute(
        builder: (BuildContext context) => new ProductDetailPage(id, prod),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    for (var i = 0; i < widget.data.length; i += 2) {
      Product prod1 = new Product(
        widget.data[i]['nama'],
        widget.data[i]['hargaBibit'],
        widget.data[i]['persen'],
        widget.data[i]['body'],
        widget.data[i]['durasi'],
        widget.data[i]['risiko'],
        widget.data[i]['rating'],
        widget.data[i]['jumlahPenjualan'],
        widget.data[i]['namaOwner'],
        widget.data[i]['jasa'],
        widget.data[i]['alat'],
        widget.data[i]['pupuk'],
        widget.data[i]['image1'],
        widget.data[i]['image2'],
        widget.data[i]['image3'],
      );
      Product prod2 = new Product(
        widget.data[i + 1]['nama'],
        widget.data[i + 1]['hargaBibit'],
        widget.data[i + 1]['persen'],
        widget.data[i + 1]['body'],
        widget.data[i + 1]['durasi'],
        widget.data[i + 1]['risiko'],
        widget.data[i + 1]['rating'],
        widget.data[i + 1]['jumlahPenjualan'],
        widget.data[i + 1]['namaOwner'],
        widget.data[i + 1]['jasa'],
        widget.data[i + 1]['alat'],
        widget.data[i + 1]['pupuk'],
        widget.data[i + 1]['image1'],
        widget.data[i + 1]['image2'],
        widget.data[i + 1]['image3'],
      );
      widgetRender.add(
        Row(
          children: <Widget>[
            new GestureDetector(
              onTap: () => _cardClick(i + 1, prod1),
              child: new HPCatalogueCard(prod1, i + 1),
            ),
            new GestureDetector(
              onTap: () => _cardClick(i + 2, prod2),
              child: new HPCatalogueCard(prod2, i + 2),
            ),
          ],
        ),
      );
    }
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: 10,
        horizontal: 5,
      ),
      child: Column(
        children: widgetRender,
      ),
    );
  }
}
