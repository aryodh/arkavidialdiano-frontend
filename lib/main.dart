import 'package:arkavidialdiano_front/pages/articleMainPage.dart';
import 'package:arkavidialdiano_front/pages/mainPage.dart';
import 'package:arkavidialdiano_front/pages/profilePage.dart';
import 'package:arkavidialdiano_front/pages/trackerMainPage.dart';
import 'package:arkavidialdiano_front/widget/QuantityWidget.dart';
import 'package:flutter/material.dart';

import 'constants.dart';

void main() => runApp(MyApp());

/// This Widget is the main application widget.
class MyApp extends StatelessWidget {
  static const String _title = 'Grow';

  static Map<int, Color> color = {
    50: Color.fromRGBO(92, 185, 129, .1),
    100: Color.fromRGBO(92, 185, 129, .2),
    200: Color.fromRGBO(92, 185, 129, .3),
    300: Color.fromRGBO(92, 185, 129, .4),
    400: Color.fromRGBO(92, 185, 129, .5),
    500: Color.fromRGBO(92, 185, 129, .6),
    600: Color.fromRGBO(92, 185, 129, .7),
    700: Color.fromRGBO(92, 185, 129, .8),
    800: Color.fromRGBO(92, 185, 129, .9),
    900: Color.fromRGBO(92, 185, 129, 1),
  };

  MaterialColor colorCustom = MaterialColor(0xFF5CB981, color);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: _title,
      home: App(),
      theme: ThemeData(primarySwatch: colorCustom),
    );
  }
}

class App extends StatefulWidget {
  App({Key key}) : super(key: key);
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  PageController _myPage;
  var selectedPage;

  @override
  void initState() {
    super.initState();
    _myPage = PageController(initialPage: 0);
    selectedPage = 0;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        physics: NeverScrollableScrollPhysics(),
        controller: _myPage,
        children: <Widget>[
          MainPage(),
          TrackerMainPage(),
          ArticleMainPage(),
          ProfilePage(),
        ],
      ),
      bottomNavigationBar: BottomAppBar(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Container(
              height: 64,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    height: 35,
                    margin: EdgeInsets.only(bottom: 3),
                    child: IconButton(
                      iconSize: 30,
                      icon: Icon(Icons.home),
                      color: selectedPage == 0
                          ? Theme.of(context).primaryColor
                          : Colors.grey,
                      onPressed: () {
                        _myPage.jumpToPage(0);
                        setState(() {
                          selectedPage = 0;
                        });
                      },
                    ),
                  ),
                  Text(
                    "Home",
                    style: TextStyle(
                      fontSize: 12,
                      color: selectedPage == 0
                          ? Theme.of(context).primaryColor
                          : Colors.grey,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: 64,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    height: 35,
                    margin: EdgeInsets.only(bottom: 3),
                    child: IconButton(
                      iconSize: 30,
                      icon: Icon(Icons.track_changes),
                      color: selectedPage == 1
                          ? Theme.of(context).primaryColor
                          : Colors.grey,
                      onPressed: () {
                        _myPage.jumpToPage(1);
                        setState(() {
                          selectedPage = 1;
                        });
                      },
                    ),
                  ),
                  Text(
                    "Tracker",
                    style: TextStyle(
                      fontSize: 12,
                      color: selectedPage == 1
                          ? Theme.of(context).primaryColor
                          : Colors.grey,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: 64,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    height: 35,
                    margin: EdgeInsets.only(bottom: 3),
                    child: IconButton(
                      iconSize: 30,
                      icon: Icon(Icons.web),
                      color: selectedPage == 2
                          ? Theme.of(context).primaryColor
                          : Colors.grey,
                      onPressed: () {
                        _myPage.jumpToPage(2);
                        setState(() {
                          selectedPage = 2;
                        });
                      },
                    ),
                  ),
                  Text(
                    "Article",
                    style: TextStyle(
                      fontSize: 12,
                      color: selectedPage == 2
                          ? Theme.of(context).primaryColor
                          : Colors.grey,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: 64,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    height: 35,
                    margin: EdgeInsets.only(bottom: 3),
                    child: IconButton(
                      iconSize: 30,
                      icon: Icon(Icons.person),
                      color: selectedPage == 3
                          ? Theme.of(context).primaryColor
                          : Colors.grey,
                      onPressed: () {
                        _myPage.jumpToPage(3);
                        setState(() {
                          selectedPage = 3;
                        });
                      },
                    ),
                  ),
                  Text(
                    "Profile",
                    style: TextStyle(
                      fontSize: 12,
                      color: selectedPage == 3
                          ? Theme.of(context).primaryColor
                          : Colors.grey,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
